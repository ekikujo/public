package es.uniovi.innova.eadministracion.internacionales.supervision.datos.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import es.uniovi.innova.eadministracion.internacionales.supervision.modelo.PeriodoNoPago;
import es.uniovi.innova.eadministracion.internacionales.supervision.modelo.UniversidadOrigen;

public class UniversidadOrigenDAO extends JdbcDaoSupport{
	private static Logger _log = Logger.getLogger(UniversidadOrigenDAO.class);

	private String obtenerUniversidadesOrigenSupervisor_Query;
	private String obtenerTodasUniversidadesOrigen_Query;
	private String obtenerUniversidadOrigenById_Query;
	
	private String insertarNuevaUniversidadOrigen_Query;
	private String actualizarUniversidadOrigen_Query;
	private String eliminarUniversidadOrigen_Query;
	
	private String insertarNuevoPeriodoNoPagoUniversidadOrigen_Query;
	private String eliminarPeriodosNoPagoByUniversidadOrigen_Query;
	
	private String obtenerPeriodosPagoUniversidadOrigen_Query;
	
	private String eliminarAsociacionesUniversidadOrigenConSupervisoresQuery;
	

	public UniversidadOrigen crearNuevaUniversidadOrigen(UniversidadOrigen universidadOrigenCrear, String cursoAcademicoActual) {
		NamedParameterJdbcTemplate npJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("nombre", universidadOrigenCrear.getNombre());
		parameters.addValue("direccionpostal", universidadOrigenCrear.getDireccionPostal());
		parameters.addValue("telefono", universidadOrigenCrear.getTelefono());
		parameters.addValue("sueldomeslector", universidadOrigenCrear.getSueldoMesLector());
		parameters.addValue("sueldomesauxiliar", universidadOrigenCrear.getSueldoMesAuxiliar());
		parameters.addValue("cursoacademico", cursoAcademicoActual);
		/*
		INSERT INTO universidadorigen(nombre, direccionpostal, telefono, sueldomesauxiliar, sueldomeslector, 	
										cursoacademico)
				VALUES (:nombre, :direccionpostal, :telefono, :sueldomesauxiliar, :sueldomeslector, :cursoacademico );
		*/
		KeyHolder keyHolder = new GeneratedKeyHolder();
		_log.trace("SQL: " + insertarNuevaUniversidadOrigen_Query);
		npJdbcTemplate.update(insertarNuevaUniversidadOrigen_Query, parameters, keyHolder, new String[]{"id"});
		Number id = keyHolder.getKey();
		universidadOrigenCrear.setId(id.intValue());
		_log.debug("Insertada nueva universidad "+universidadOrigenCrear );
		return universidadOrigenCrear;
	}

	
	public UniversidadOrigen actualizarUniversidadOrigen(UniversidadOrigen universidadOrigenActualizar) {
		NamedParameterJdbcTemplate npJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("nombre", universidadOrigenActualizar.getNombre());
		parameters.put("direccionpostal", universidadOrigenActualizar.getDireccionPostal());
		parameters.put("telefono", universidadOrigenActualizar.getTelefono());
		parameters.put("sueldomeslector", universidadOrigenActualizar.getSueldoMesLector());
		parameters.put("sueldomesauxiliar", universidadOrigenActualizar.getSueldoMesAuxiliar());
		parameters.put("id", universidadOrigenActualizar.getId());
		
		/*
		UPDATE universidadorigen
		   SET  nombre=:nombre, sueldomesauxiliar=:sueldomesauxiliar, sueldomeslector=:sueldomeslector, 
		       direccionpostal=:direccionpostal, telefono=:telefono
		 WHERE id=:id;

		*/
		
		_log.trace("SQL: " + actualizarUniversidadOrigen_Query);
		npJdbcTemplate.update(actualizarUniversidadOrigen_Query, parameters);
		
		_log.debug("Actualizada universidad "+universidadOrigenActualizar );
		return universidadOrigenActualizar;
	}



	public void eliminar(UniversidadOrigen universidadOrigenEliminar) {
		NamedParameterJdbcTemplate npJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", universidadOrigenEliminar.getId());
		
		/*
			DELETE FROM universidadorigen
		 		WHERE id=:id;

		*/
		
		_log.trace("SQL: " + eliminarUniversidadOrigen_Query);
		npJdbcTemplate.update(eliminarUniversidadOrigen_Query, parameters);
		
		_log.debug("Eliminada universidad "+universidadOrigenEliminar );
		
	}
	

	
	public UniversidadOrigen obtenerUniversidadOrigenById(long id) throws CannotGetJdbcConnectionException {
		_log.debug("Obteniendo universidad de origen con id: " + id);
		NamedParameterJdbcTemplate npJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", id);
		
		UniversidadOrigen universidadOrigen = new UniversidadOrigen();
		
		/*
		SELECT id, nombre, sueldomesauxiliar, sueldomeslector, cursoacademico
		  FROM universidadorigen
		  WHERe id = :id
		 */
		
		try {
			_log.trace("SQL: " + obtenerUniversidadOrigenById_Query);
			
			
			universidadOrigen = npJdbcTemplate.query(obtenerUniversidadOrigenById_Query, parameters, new ResultSetExtractor<UniversidadOrigen>() {
				@Override
				public UniversidadOrigen extractData(ResultSet rs) throws SQLException, DataAccessException {
					UniversidadOrigen universidadOrigen = new UniversidadOrigen();
					while (rs.next()) {
						try {
							universidadOrigen = obtenerUniversidadOrigenByResultSet(rs);
							break;
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					return universidadOrigen;
				}
			});
		} catch (CannotGetJdbcConnectionException e) {
			throw new CannotGetJdbcConnectionException(e.getMessage() + ". Causa: " + e.getCause(), new SQLException(e));
		}
		/*
		catch (Exception e) {
			throw new Exception("Problemas con la conexion a la base de datos o recuperacion de datos. Mensaje: " + e.getMessage() + ". Causa: " + e.getCause(), e);
		}
		*/
		_log.debug("Se devuelven universidad de origen con id: " + id);
		return universidadOrigen;
	}

	
	
	public List<UniversidadOrigen> obtenerUniversidadesOrigenSupervisor(String uidSupervisor) throws CannotGetJdbcConnectionException, Exception {
		_log.debug("Obteniendo universidades origen que controla el supervisor con uid: " + uidSupervisor);
		NamedParameterJdbcTemplate npJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
		Map<String, Object> parameters = new HashMap<String, Object>();
	
		parameters.put("uidSupervisor", uidSupervisor);
		
		List<UniversidadOrigen> listaUniversidadesOrigen = new ArrayList<UniversidadOrigen>();
		
		/*
		 	

		 */
		
		try {
			_log.trace("SQL: " + obtenerUniversidadesOrigenSupervisor_Query);
			listaUniversidadesOrigen = npJdbcTemplate.query(obtenerUniversidadesOrigenSupervisor_Query, parameters, new ResultSetExtractor<List<UniversidadOrigen>>() {
				@Override
				public List<UniversidadOrigen> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<UniversidadOrigen> listaUniversidadesOrigen = new ArrayList<UniversidadOrigen>();
					while (rs.next()) {
						
						UniversidadOrigen universidadOrigen;
						try {
							universidadOrigen = obtenerUniversidadOrigenByResultSet(rs);
							listaUniversidadesOrigen.add(universidadOrigen);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
					}
					return listaUniversidadesOrigen;
				}
			});
		} catch (CannotGetJdbcConnectionException e) {
			throw new CannotGetJdbcConnectionException(e.getMessage() + ". Causa: " + e.getCause(), new SQLException(e));
		} catch (Exception e) {
			throw new Exception("Problemas con la conexion a la base de datos o recuperacion de datos. Mensaje: " + e.getMessage() + ". Causa: " + e.getCause(), e);
		}
		_log.debug("Se devuelven resultados de las universidades origen que controla el supervisor con uid: " + uidSupervisor);
		return listaUniversidadesOrigen;
	}
	
	

	public List<PeriodoNoPago> crearNuevoPeriodoNoPago(UniversidadOrigen universidadOrigenCrear) {
		List<PeriodoNoPago> listaPeridoosNoPago = universidadOrigenCrear.getListaPeriodosNoPago();
		NamedParameterJdbcTemplate npJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
		for(PeriodoNoPago p: listaPeridoosNoPago){
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("universidadorigenid", universidadOrigenCrear.getId());
			parameters.put("fechainicio", p.getFechaInicio());
			parameters.put("fechafin", p.getFechaFin());
			/*
			INSERT INTO periodonopago(universidadorigenid, fechainicio, fechafin)
	    	VALUES (:universidadorigenid, :fechainicio, :fechafin);
			*/
			
			_log.trace("SQL: " + insertarNuevoPeriodoNoPagoUniversidadOrigen_Query);
			npJdbcTemplate.update(insertarNuevoPeriodoNoPagoUniversidadOrigen_Query, parameters);
			
			_log.debug("Insertado nuevo periodo no pago "+p );
		}
		return listaPeridoosNoPago;
	}
	
	public void eliminarPeriodosNoPago(UniversidadOrigen universidadOrigenEliminar) {
		NamedParameterJdbcTemplate npJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("universidadorigenid", universidadOrigenEliminar.getId());
		
		/*
			DELETE FROM periodonopago
		 		WHERE universidadorigenid=:universidadorigenid;
		*/
		
		_log.trace("SQL: " + eliminarPeriodosNoPagoByUniversidadOrigen_Query);
		npJdbcTemplate.update(eliminarPeriodosNoPagoByUniversidadOrigen_Query, parameters);
		
		_log.debug("Eliminados periodos no pago de la universidad "+universidadOrigenEliminar );
	}
	
	
	public List<PeriodoNoPago> obtenerPeriodosNoPagoUniversidadOrigen(long universidadorigenid) throws Exception {
		_log.debug("Obteniendo todos los periodos de no pago de la universidad origen con id : " + universidadorigenid);
		NamedParameterJdbcTemplate npJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("universidadorigenid", universidadorigenid);
		List<PeriodoNoPago> listaPeriodosNoPago = new ArrayList<PeriodoNoPago>();
		
		/*
		 	SELECT universidadorigenid, fechainicio, fechafin
				FROM periodonopago;
				WHERE universidadorigenid = :universidadorigenid
		 */
		
		try {
			_log.trace("SQL: " + obtenerPeriodosPagoUniversidadOrigen_Query);
			listaPeriodosNoPago = npJdbcTemplate.query(obtenerPeriodosPagoUniversidadOrigen_Query, parameters, new ResultSetExtractor<List<PeriodoNoPago>>() {
				@Override
				public List<PeriodoNoPago> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<PeriodoNoPago> listaPeriodosNoPago = new ArrayList<PeriodoNoPago>();
					while (rs.next()) {
						long periodoNoPagoId = rs.getLong("periodoNoPagoId");
						long universidadorigenid = rs.getLong("universidadorigenid");
						Date fechaInicio = rs.getDate("fechainicio");
						Date fechaFin = rs.getDate("fechafin");
						
						PeriodoNoPago periodoNoPago = new PeriodoNoPago(universidadorigenid, fechaInicio, fechaFin);
						periodoNoPago.setId(periodoNoPagoId);
						listaPeriodosNoPago.add(periodoNoPago);
						
					}
					return listaPeriodosNoPago;
				}
			});
		} catch (CannotGetJdbcConnectionException e) {
			throw new CannotGetJdbcConnectionException(e.getMessage() + ". Causa: " + e.getCause(), new SQLException(e));
		} catch (Exception e) {
			throw new Exception("Problemas con la conexion a la base de datos o recuperacion de datos. Mensaje: " + e.getMessage() + ". Causa: " + e.getCause(), e);
		}
		_log.debug("Se devuelven resultados de todos los periodos de no pago de la universidad origen con id : " + universidadorigenid);
		return listaPeriodosNoPago;
	}

	
	public List<UniversidadOrigen> obtenerTodasUniversidadesOrigen(String cursoAcademicoActual) throws Exception {
		_log.debug("Obteniendo todas las universidades origen ");
		NamedParameterJdbcTemplate npJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("cursoAcademico", cursoAcademicoActual);
		List<UniversidadOrigen> listaUniversidadesOrigen = new ArrayList<UniversidadOrigen>();
		
		/*
		 	SELECT id, nombre, sueldomesauxiliar, sueldomeslector, cursoacademico
  			FROM universidadorigen
  			WHERE cursoacademico=:cursoAcademico
		 */
		
		try {
			_log.trace("SQL: " + obtenerTodasUniversidadesOrigen_Query);
			listaUniversidadesOrigen = npJdbcTemplate.query(obtenerTodasUniversidadesOrigen_Query, parameters, new ResultSetExtractor<List<UniversidadOrigen>>() {
				@Override
				public List<UniversidadOrigen> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<UniversidadOrigen> listaUniversidadesOrigen = new ArrayList<UniversidadOrigen>();
					while (rs.next()) {
						
						UniversidadOrigen universidadOrigen;
						try {
							universidadOrigen = obtenerUniversidadOrigenByResultSet(rs);
							listaUniversidadesOrigen.add(universidadOrigen);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
					}
					return listaUniversidadesOrigen;
				}
			});
		} catch (CannotGetJdbcConnectionException e) {
			throw new CannotGetJdbcConnectionException(e.getMessage() + ". Causa: " + e.getCause(), new SQLException(e));
		} catch (Exception e) {
			throw new Exception("Problemas con la conexion a la base de datos o recuperacion de datos. Mensaje: " + e.getMessage() + ". Causa: " + e.getCause(), e);
		}
		_log.debug("Se devuelven resultados de todas las universidades origen");
		return listaUniversidadesOrigen;
	}
	
	public void eliminarAsociacionesConSupervisores(UniversidadOrigen universidadOrigen) {
		_log.info("Eliminando las asociaciones entre la Universidad de Origen cuyo ID es " + universidadOrigen.getId() + " y todos sus supervisores...");
		
		NamedParameterJdbcTemplate npJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("universidadorigenid", universidadOrigen.getId());
		
		npJdbcTemplate.update(eliminarAsociacionesUniversidadOrigenConSupervisoresQuery, parameters);
		
		_log.debug("...asociaciones eliminadas con éxito");
		
	}
	
	
	private UniversidadOrigen obtenerUniversidadOrigenByResultSet(ResultSet rs) throws Exception{
		long id = rs.getInt("id");
		
		String nombre = rs.getString("nombre");
		String direccionpostal = rs.getString("direccionpostal");
		String telefono = rs.getString("telefono");
		float sueldomeslector = rs.getFloat("sueldomeslector");
		float sueldomesauxiliar = rs.getFloat("sueldomesauxiliar");
		String cursoacademico = rs.getString("cursoacademico");
		
		UniversidadOrigen universidadOrigen = new UniversidadOrigen(nombre, direccionpostal, telefono, sueldomeslector, sueldomesauxiliar, cursoacademico);
		universidadOrigen.setId(id);
		List<PeriodoNoPago> listaPeriodosNoPago = obtenerPeriodosNoPagoUniversidadOrigen(id);
		universidadOrigen.setListaPeriodosNoPago(listaPeriodosNoPago);
		
		return universidadOrigen;
	}


	
	
	public String getObtenerUniversidadesOrigenSupervisor_Query() {
		return obtenerUniversidadesOrigenSupervisor_Query;
	}

	public void setObtenerUniversidadesOrigenSupervisor_Query(String obtenerUniversidadesOrigenSupervisor_Query) {
		this.obtenerUniversidadesOrigenSupervisor_Query = obtenerUniversidadesOrigenSupervisor_Query;
	}



	public String getObtenerTodasUniversidadesOrigen_Query() {
		return obtenerTodasUniversidadesOrigen_Query;
	}



	public void setObtenerTodasUniversidadesOrigen_Query(String obtenerTodasUniversidadesOrigen_Query) {
		this.obtenerTodasUniversidadesOrigen_Query = obtenerTodasUniversidadesOrigen_Query;
	}



	public String getObtenerUniversidadOrigenById_Query() {
		return obtenerUniversidadOrigenById_Query;
	}



	public void setObtenerUniversidadOrigenById_Query(String obtenerUniversidadOrigenById_Query) {
		this.obtenerUniversidadOrigenById_Query = obtenerUniversidadOrigenById_Query;
	}



	public String getInsertarNuevaUniversidadOrigen_Query() {
		return insertarNuevaUniversidadOrigen_Query;
	}



	public void setInsertarNuevaUniversidadOrigen_Query(String insertarNuevaUniversidadOrigen_Query) {
		this.insertarNuevaUniversidadOrigen_Query = insertarNuevaUniversidadOrigen_Query;
	}



	public String getObtenerPeriodosPagoUniversidadOrigen_Query() {
		return obtenerPeriodosPagoUniversidadOrigen_Query;
	}



	public void setObtenerPeriodosPagoUniversidadOrigen_Query(String obtenerPeriodosPagoUniversidadOrigen_Query) {
		this.obtenerPeriodosPagoUniversidadOrigen_Query = obtenerPeriodosPagoUniversidadOrigen_Query;
	}


	public String getActualizarUniversidadOrigen_Query() {
		return actualizarUniversidadOrigen_Query;
	}


	public void setActualizarUniversidadOrigen_Query(String actualizarUniversidadOrigen_Query) {
		this.actualizarUniversidadOrigen_Query = actualizarUniversidadOrigen_Query;
	}


	public String getEliminarUniversidadOrigen_Query() {
		return eliminarUniversidadOrigen_Query;
	}


	public void setEliminarUniversidadOrigen_Query(String eliminarUniversidadOrigen_Query) {
		this.eliminarUniversidadOrigen_Query = eliminarUniversidadOrigen_Query;
	}


	public String getInsertarNuevoPeriodoNoPagoUniversidadOrigen_Query() {
		return insertarNuevoPeriodoNoPagoUniversidadOrigen_Query;
	}


	public void setInsertarNuevoPeriodoNoPagoUniversidadOrigen_Query(
			String insertarNuevoPeriodoNoPagoUniversidadOrigen_Query) {
		this.insertarNuevoPeriodoNoPagoUniversidadOrigen_Query = insertarNuevoPeriodoNoPagoUniversidadOrigen_Query;
	}


	public String getEliminarPeriodosNoPagoByUniversidadOrigen_Query() {
		return eliminarPeriodosNoPagoByUniversidadOrigen_Query;
	}


	public void setEliminarPeriodosNoPagoByUniversidadOrigen_Query(String eliminarPeriodosNoPagoByUniversidadOrigen_Query) {
		this.eliminarPeriodosNoPagoByUniversidadOrigen_Query = eliminarPeriodosNoPagoByUniversidadOrigen_Query;
	}

	public void setEliminarAsociacionesUniversidadOrigenConSupervisoresQuery(String eliminarAsociacionesUniversidadOrigenConSupervisoresQuery) {
		this.eliminarAsociacionesUniversidadOrigenConSupervisoresQuery = eliminarAsociacionesUniversidadOrigenConSupervisoresQuery;
	}
	
}
