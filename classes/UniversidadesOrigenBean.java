package es.uniovi.innova.eadministracion.internacionales.supervision.presentacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.CannotGetJdbcConnectionException;

import es.uniovi.innova.eadministracion.internacionales.supervision.modelo.PeriodoNoPago;
import es.uniovi.innova.eadministracion.internacionales.supervision.modelo.Supervisor;
import es.uniovi.innova.eadministracion.internacionales.supervision.modelo.UniversidadOrigen;
import es.uniovi.innova.eadministracion.internacionales.supervision.modelo.tipos.TipoPersona;
import es.uniovi.innova.eadministracion.internacionales.supervision.negocio.impl.SupervisorService;
import es.uniovi.innova.eadministracion.internacionales.supervision.negocio.impl.UniversidadOrigenService;
import es.uniovi.innova.eadministracion.internacionales.supervision.security.PermisoSecurityUtils;
import es.uniovi.innova.eadministracion.internacionales.supervision.util.FechasUtil;
import es.uniovi.innova.eadministracion.internacionales.supervision.util.Utils;

public class UniversidadesOrigenBean implements Serializable{
	private static final long serialVersionUID = -2730638577228418884L;
	
	private static Logger log = Logger.getLogger(UniversidadesOrigenBean.class);
	
	@Autowired
	private UniversidadOrigenService universidadOrigenService;
	@Autowired
	private SupervisorService supervisorService;
	
	private List<UniversidadOrigen> listaUniversidadesOrigen;
	private List<UniversidadOrigen> listaUniversidadesOrigenFiltradas;
	private UniversidadOrigen universidadOrigenCrear;
	private UniversidadOrigen universidadOrigenActualizar;
	private UniversidadOrigen universidadOrigenVer;
	private List<PeriodoNoPago> listaPeriodosNoPagoSeleccionadas;
	private Date fechaInicioPeriodoNoPago;
	private Date fechaFinPeriodoNoPago;
	private List<Supervisor> listaSupervisoresUniversidadOrigen;
	private Supervisor supervisorUniversidadOrigenCrear;
	private Supervisor supervisorUniversidadOrigenActualizar;
	private Supervisor supervisorUniversidadOrigenActualizarAntiguo;
	
	private String cursoAcademico ="";
	private Supervisor usuarioActual;
	
	private FechasUtil fechasUtil = new FechasUtil();
	
	
	@PostConstruct
	public void initBean() {
		try 
		{
			if(cursoAcademico.equals(""))
			{
				cursoAcademico = Utils.getCursoAcademicoActual();
			}
			
			obtenerUsuarioActual();
			
			listaUniversidadesOrigen = universidadOrigenService.obtenerTodasUniversidadesOrigen(cursoAcademico);
			listaUniversidadesOrigenFiltradas = new ArrayList<UniversidadOrigen>(this.listaUniversidadesOrigen);
			listaPeriodosNoPagoSeleccionadas = new ArrayList<PeriodoNoPago>();
			
			universidadOrigenCrear = new UniversidadOrigen();
			universidadOrigenActualizar = new UniversidadOrigen();
			universidadOrigenVer = new UniversidadOrigen();
			listaSupervisoresUniversidadOrigen = supervisorService.obtenerTodosSupervisoresDeUniversidadOrigen();
			supervisorUniversidadOrigenCrear = null;
			supervisorUniversidadOrigenActualizar = null;
			supervisorUniversidadOrigenActualizarAntiguo = null;
			
			long universidadOrigenVerId = (Long) ((FacesContext.getCurrentInstance().getExternalContext().getFlash().get("universidadOrigenVerId")!=null?FacesContext.getCurrentInstance().getExternalContext().getFlash().get("universidadOrigenVerId"):-1l));
			if(universidadOrigenVerId>0)
			{
				universidadOrigenVer = universidadOrigenService.obtenerUniversidadOrigenById(universidadOrigenVerId);
			}
			long universidadOrigenActualizarId = (Long) ((FacesContext.getCurrentInstance().getExternalContext().getFlash().get("universidadOrigenActualizarId")!=null?FacesContext.getCurrentInstance().getExternalContext().getFlash().get("universidadOrigenActualizarId"):-1l));
			if(universidadOrigenActualizarId>0)
			{
				universidadOrigenActualizar = universidadOrigenService.obtenerUniversidadOrigenById(universidadOrigenActualizarId);
				supervisorUniversidadOrigenActualizar = supervisorService.obtenerSupervisorByUniversidadOrigen(universidadOrigenActualizarId);
				supervisorUniversidadOrigenActualizarAntiguo = supervisorUniversidadOrigenActualizar;
			}
		}
		catch(Exception e) 
		{
			log.error("Se ha producido un error durante la carga inicial de los datos.\n" + e.getCause() + "\n" + e.getMessage());
		}
	}
	
	public void obtenerUsuarioActual() throws CannotGetJdbcConnectionException, Exception {
		if (this.usuarioActual == null) {
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
			HttpSession session = ((HttpServletRequest) context.getRequest()).getSession();
			this.usuarioActual = PermisoSecurityUtils.obtenerUsuarioRegistroSesion();
			session.setAttribute("nombrePersonaEnSesion", usuarioActual.getNombre());
			session.setAttribute("apellidosPersonaEnSesion", usuarioActual.getApellidos());
		}	
	}
	
	public TipoPersona[] getTiposPersona(){
		//TipoPersona[] tipos = new TipoPersona[TipoPersona.values().length -1];
		TipoPersona[] tipos = new TipoPersona[TipoPersona.values().length];
		int index=0;
		for (TipoPersona tipo : TipoPersona.values()) {
			//if(!tipo.equals(TipoPersona.ADMINISTRADOR)){
				tipos[index++] = tipo;
			//}
	    }
		return tipos;
	}
	
	public String crearNuevaUniversidadOrigen() {
		universidadOrigenCrear = universidadOrigenService.crearNuevaUniversidadOrigen(universidadOrigenCrear, supervisorUniversidadOrigenCrear, Utils.getCursoAcademicoActual());
		log.info("Universidad de Origen '" + universidadOrigenCrear.getNombre() + "' creada satifactoriamente");
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Universidad de Origen creada satisfactoriamente", "Universidad de Origen creada satisfactoriamente"));
		return volverListadoUniversidadesOrigen();
	}
	
	public String actualizarUniversidadOrigen() {
		universidadOrigenActualizar = universidadOrigenService.actualizarUniversidadOrigen(universidadOrigenActualizar, supervisorUniversidadOrigenActualizarAntiguo, supervisorUniversidadOrigenActualizar);
		log.info("Universidad de Origen '" + universidadOrigenActualizar.getNombre() + "' actualizada satifactoriamente");
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Datos de la Universidad de Origen actualizados satisfactoriamente", "Datos de la Universidad de Origen actualizados satisfactoriamente"));
		return volverListadoUniversidadesOrigen();
	}
	
	public String eliminarUniversidadOrigen(UniversidadOrigen universidadOrigenEliminar) {
		try
		{
			universidadOrigenService.eliminar(universidadOrigenEliminar);
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Universidad de Origen eliminada correctamente", "Universidad de Origen eliminada correctamente");
			FacesContext.getCurrentInstance().addMessage(null, message);
			return volverListadoUniversidadesOrigen();
		}
		catch (Exception e) 
		{
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar la Universidad de Origen ya que tiene asociada algún Lector o Auxiliar", "No se puede eliminar la Universidad de Origen ya que tiene asociada algún Lector o Auxiliar");
			FacesContext.getCurrentInstance().addMessage(null, message);
			return "";
		}
	}
	
	public void agregarPeriodoNoPagoUniversidad(boolean crear){
		if(fechaInicioPeriodoNoPago != null && fechaFinPeriodoNoPago != null) 
		{
			PeriodoNoPago periodoNoPago = new PeriodoNoPago(fechaInicioPeriodoNoPago, fechaFinPeriodoNoPago);
			
			DateTime fechaInicioDateTime = new DateTime(fechaInicioPeriodoNoPago);
			DateTime fechaFinDateTime = new DateTime(fechaFinPeriodoNoPago);
	
			UniversidadOrigen universidadOrigenAux = universidadOrigenActualizar;
			if(crear) 
			{
				universidadOrigenAux = universidadOrigenCrear;
			}
			
			log.info("Añadiendo periodo de no pago (" + fechaInicioPeriodoNoPago + " - " + fechaFinPeriodoNoPago + ") a la Universidad " + universidadOrigenAux.getNombre());
			
			if(!universidadOrigenAux.getListaPeriodosNoPago().contains(periodoNoPago) && (!fechaFinDateTime.isBefore(fechaInicioDateTime))) 
			{
				//El periodo de pago debe tener como fecha de inicio y fin el mismo mes y el mismo año
				if(fechaInicioDateTime.getMonthOfYear() == fechaFinDateTime.getMonthOfYear() 
						&& fechaInicioDateTime.getYear() == fechaFinDateTime.getYear())
				{
					universidadOrigenAux.getListaPeriodosNoPago().add(periodoNoPago);
					listaPeriodosNoPagoSeleccionadas = new ArrayList<PeriodoNoPago>();
					fechaFinPeriodoNoPago = null;
					fechaInicioPeriodoNoPago = null;
					universidadOrigenCrear = universidadOrigenAux;
					universidadOrigenActualizar = universidadOrigenAux;
				}
				else
				{
					log.error("El periodo de pago debe tener como fecha de inicio y fecha de fin el mismo mes y el mismo año");
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El periodo de pago debe tener como fecha de inicio y fecha de fin el mismo mes y el mismo año", "El periodo de pago debe tener como fecha de inicio y fecha de fin el mismo mes y el mismo año"));
				}
			} 
			else if(fechaInicioDateTime.isAfter(fechaFinDateTime)) 
			{
				log.error("La fecha de inicio del periodo de no pago no puede ser posterior a la fecha de finalización");
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha de inicio del periodo de no pago no puede ser posterior a la fecha de finalización", "La fecha de inicio del periodo de no pago no puede ser posterior a la fecha de finalización"));
			} 
			else 
			{
				log.error("No se pueden indicar dos periodos de no pago con la misma fecha de inicio");
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se pueden indicar dos periodos de no pago con la misma fecha de inicio", "No se pueden indicar dos periodos de no pago con la misma fecha de inicio"));
			}
		} 
		else 
		{
			log.error("Las fechas de inicio y finalización del periodo de no pago no pueden ser nulas");
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Las fechas de inicio y finalización del periodo de no pago no pueden ser nulas", "Las fechas de inicio y finalización del periodo de no pago no pueden ser nulas"));
		}
	}
	
	
	public void eliminarPeriodoNoPagoUniversidad(boolean crear){
		UniversidadOrigen universidadOrigenAux = universidadOrigenActualizar;
		if(crear){
			universidadOrigenAux = universidadOrigenCrear;
		}
		for(PeriodoNoPago periodoNoPago: listaPeriodosNoPagoSeleccionadas){
			universidadOrigenAux.getListaPeriodosNoPago().remove(periodoNoPago);
		}
		universidadOrigenCrear = universidadOrigenAux;
		universidadOrigenActualizar = universidadOrigenAux;
		listaPeriodosNoPagoSeleccionadas = new ArrayList<PeriodoNoPago>();
		fechaFinPeriodoNoPago = null;
		fechaInicioPeriodoNoPago = null;
	}
	
	public String setUniversidadOrigenSeleccionada(UniversidadOrigen universidadOrigenVer) {
		this.universidadOrigenVer = universidadOrigenVer;
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("universidadOrigenVerId", universidadOrigenVer.getId());
		return verDetalleUniversidadOrigen();
	}

	public String setActualizarUniversidadOrigen(UniversidadOrigen universidadOrigenActualizar) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("universidadOrigenActualizarId", universidadOrigenActualizar.getId());
		this.universidadOrigenActualizar = universidadOrigenActualizar;
		return irAActualizarUniversidadOrigen();
	}
	
	
	//NAVIGATION
	public String irACrearUniversidadOrigen(){
		return "irACrearUniversidadOrigen";
	}
	
	public String volverListadoUniversidadesOrigen(){
		return "volverListadoUniversidadesOrigen";
	}
	
	public String verDetalleUniversidadOrigen(){
		return "verDetalleUniversidadOrigen";
	}
	
	public String irAActualizarUniversidadOrigen(){
		return "irAActualizarUniversidadOrigen";
	}
	
	//GETTER y SETTER
	public Supervisor getUsuarioActual() {
		return usuarioActual;
	}

	public String getCursoAcademico() {
		return cursoAcademico;
	}

	public void setCursoAcademico(String cursoAcademico) {
		this.cursoAcademico = cursoAcademico;
	}

	public Supervisor getUsuarioAdministradorActual() {
		return usuarioActual;
	}

	public void setUsuarioAdministradorActual(Supervisor usuarioAdministradorActual) {
		this.usuarioActual = usuarioAdministradorActual;
	}

	public List<UniversidadOrigen> getListaUniversidadesOrigenFiltradas() {
		return listaUniversidadesOrigenFiltradas;
	}

	public void setListaUniversidadesOrigenFiltradas(List<UniversidadOrigen> listaUniversidadesOrigenFiltradas) {
		this.listaUniversidadesOrigenFiltradas = listaUniversidadesOrigenFiltradas;
	}

	public List<UniversidadOrigen> getListaUniversidadesOrigen() {
		return listaUniversidadesOrigen;
	}

	public void setListaUniversidadesOrigen(List<UniversidadOrigen> listaUniversidadesOrigen) {
		this.listaUniversidadesOrigen = listaUniversidadesOrigen;
	}

	public UniversidadOrigen getUniversidadOrigenCrear() {
		return universidadOrigenCrear;
	}

	public void setUniversidadOrigenCrear(UniversidadOrigen universidadOrigenCrear) {
		this.universidadOrigenCrear = universidadOrigenCrear;
	}

	public UniversidadOrigen getUniversidadOrigenActualizar() {
		return universidadOrigenActualizar;
	}

	public void setUniversidadOrigenActualizar(UniversidadOrigen universidadOrigenActualizar) {
		this.universidadOrigenActualizar = universidadOrigenActualizar;
	}

	public UniversidadOrigen getUniversidadOrigenVer() {
		return universidadOrigenVer;
	}

	public void setUniversidadOrigenVer(UniversidadOrigen universidadOrigenVer) {
		this.universidadOrigenVer = universidadOrigenVer;
	}

	public List<PeriodoNoPago> getListaPeriodosNoPagoSeleccionadas() {
		return listaPeriodosNoPagoSeleccionadas;
	}

	public void setListaPeriodosNoPagoSeleccionadas(List<PeriodoNoPago> listaPeriodosNoPagoSeleccionadas) {
		this.listaPeriodosNoPagoSeleccionadas = listaPeriodosNoPagoSeleccionadas;
	}

	public Date getFechaInicioPeriodoNoPago() {
		return fechaInicioPeriodoNoPago;
	}

	public void setFechaInicioPeriodoNoPago(Date fechaInicioPeriodoNoPago) {
		this.fechaInicioPeriodoNoPago = fechaInicioPeriodoNoPago;
	}

	public Date getFechaFinPeriodoNoPago() {
		return fechaFinPeriodoNoPago;
	}

	public void setFechaFinPeriodoNoPago(Date fechaFinPeriodoNoPago) {
		this.fechaFinPeriodoNoPago = fechaFinPeriodoNoPago;
	}

	public List<Supervisor> getListaSupervisoresUniversidadOrigen() {
		return listaSupervisoresUniversidadOrigen;
	}

	public void setListaSupervisoresUniversidadOrigen(
			List<Supervisor> listaSupervisoresUniversidadOrigen) {
		this.listaSupervisoresUniversidadOrigen = listaSupervisoresUniversidadOrigen;
	}

	public Supervisor getSupervisorUniversidadOrigenCrear() {
		return supervisorUniversidadOrigenCrear;
	}

	public void setSupervisorUniversidadOrigenCrear(Supervisor supervisorUniversidadOrigenCrear) {
		this.supervisorUniversidadOrigenCrear = supervisorUniversidadOrigenCrear;
	}

	public Supervisor getSupervisorUniversidadOrigenActualizar() {
		return supervisorUniversidadOrigenActualizar;
	}

	public void setSupervisorUniversidadOrigenActualizar(Supervisor supervisorUniversidadOrigenActualizar) {
		this.supervisorUniversidadOrigenActualizar = supervisorUniversidadOrigenActualizar;
	}

	public FechasUtil getFechasUtil() {
		return fechasUtil;
	}

}
