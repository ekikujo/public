package es.uniovi.innova.eadministracion.internacionales.supervision.negocio.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.CannotGetJdbcConnectionException;

import es.uniovi.innova.eadministracion.internacionales.supervision.datos.impl.SupervisorDAO;
import es.uniovi.innova.eadministracion.internacionales.supervision.datos.impl.UniversidadOrigenDAO;
import es.uniovi.innova.eadministracion.internacionales.supervision.modelo.PeriodoNoPago;
import es.uniovi.innova.eadministracion.internacionales.supervision.modelo.Supervisor;
import es.uniovi.innova.eadministracion.internacionales.supervision.modelo.UniversidadOrigen;

public class UniversidadOrigenService {

	private static Logger _log = Logger.getLogger(UniversidadOrigenService.class);
	
	private UniversidadOrigenDAO universidadOrigenDAO;
	private SupervisorDAO supervisorDAO;

	public UniversidadOrigenDAO getUniversidadOrigenDAO() {
		return universidadOrigenDAO;
	}

	public void setUniversidadOrigenDAO(UniversidadOrigenDAO universidadOrigenDAO) {
		this.universidadOrigenDAO = universidadOrigenDAO;
	}

	public SupervisorDAO getSupervisorDAO() {
		return supervisorDAO;
	}

	public void setSupervisorDAO(SupervisorDAO supervisorDAO) {
		this.supervisorDAO = supervisorDAO;
	}
	
	
	public UniversidadOrigen obtenerUniversidadOrigenById(long id) throws Exception {
		return  universidadOrigenDAO.obtenerUniversidadOrigenById(id);
	}
	
	public List<UniversidadOrigen> obtenerUniversidadesOrigenSupervisor(String uidSupervisor) throws CannotGetJdbcConnectionException, Exception {
		return  universidadOrigenDAO.obtenerUniversidadesOrigenSupervisor(uidSupervisor);
	}

	public List<UniversidadOrigen> obtenerTodasUniversidadesOrigen(String cursoAcademicoActual) throws Exception  {
		return universidadOrigenDAO.obtenerTodasUniversidadesOrigen(cursoAcademicoActual);
	}

	public UniversidadOrigen crearNuevaUniversidadOrigen(UniversidadOrigen universidadOrigenCrear, Supervisor supervisor, String cursoAcademicoActual) {
		universidadOrigenCrear = universidadOrigenDAO.crearNuevaUniversidadOrigen(universidadOrigenCrear, cursoAcademicoActual);
		List<PeriodoNoPago> listaPeriodosNoPagoGuardados = universidadOrigenDAO.crearNuevoPeriodoNoPago(universidadOrigenCrear);
		universidadOrigenCrear.setListaPeriodosNoPago(listaPeriodosNoPagoGuardados);
		
		if(supervisor != null)
		{
			supervisorDAO.asociarSupervisorUniversidadOrigen(supervisor.getUid(), universidadOrigenCrear.getId());
		}
		
		return universidadOrigenCrear;
	}

	public UniversidadOrigen actualizarUniversidadOrigen(UniversidadOrigen universidadOrigenActualizar, Supervisor supervisorAnterior, Supervisor supervisorNuevo) {
		universidadOrigenDAO.eliminarPeriodosNoPago(universidadOrigenActualizar);
		List<PeriodoNoPago> listaPeriodosNoPagoGuardados = universidadOrigenDAO.crearNuevoPeriodoNoPago(universidadOrigenActualizar);
		universidadOrigenActualizar.setListaPeriodosNoPago(listaPeriodosNoPagoGuardados);
		
		if(supervisorAnterior != null)
		{
			supervisorDAO.desasociarSupervisorUniversidadOrigen(supervisorAnterior.getUid(), universidadOrigenActualizar.getId());
		}
		if(supervisorNuevo != null)
		{
			supervisorDAO.asociarSupervisorUniversidadOrigen(supervisorNuevo.getUid(), universidadOrigenActualizar.getId());
		}
		
		return universidadOrigenDAO.actualizarUniversidadOrigen(universidadOrigenActualizar);
	}

	public void eliminar(UniversidadOrigen universidadOrigenEliminar) {
		universidadOrigenDAO.eliminarAsociacionesConSupervisores(universidadOrigenEliminar);
		universidadOrigenDAO.eliminarPeriodosNoPago(universidadOrigenEliminar);
		universidadOrigenDAO.eliminar(universidadOrigenEliminar);
	}
	
}
